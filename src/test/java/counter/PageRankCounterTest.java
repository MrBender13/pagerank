package counter;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static java.lang.Math.round;
import static java.lang.Math.pow;
import static org.junit.jupiter.api.Assertions.*;

class PageRankCounterTest {
    private final static float DAMPING_FACTOR = 0.7f;
    private final static int DECIMAL_PLACE = 4;
    private final static double ANSWER_SCALE = pow(10, DECIMAL_PLACE);

    @Test
    void testIsAcceptableEpsUsualCaseTrue() {
        final double epsilon = 5d;
        final List<Double> oldVector = List.of(0.5d, 1.2d, 0.003d, 23d, 0.09d);
        final List<Double> newVector = List.of(1.7d, 3.5d, 0.93d, 26d, 3d);

        assertTrue(new PageRankCounter().isAcceptableEps(oldVector, newVector, epsilon));
    }

    @Test
    void testIsAcceptableEpsUsualCaseFalse() {
        final double epsilon = 0.003d;
        final List<Double> oldVector = List.of(0.5d, 1.2d, 0.003d, 23d, 0.09d);
        final List<Double> newVector = List.of(1.7d, 22d, 0.93d);

        assertFalse(new PageRankCounter().isAcceptableEps(oldVector, newVector, epsilon));
    }

    @Test
    void testIsAcceptableEpsEmptyVectorsCaseEpsEqualsOne() {
        final double epsilon = 1d;
        final List<Double> oldVector = List.of();
        final List<Double> newVector = List.of();

        assertTrue(new PageRankCounter().isAcceptableEps(oldVector, newVector, epsilon));
    }

    @Test
    void testIsAcceptableEpsEmptyVectorsCaseZeroEps() {
        final double epsilon = 0d;
        final List<Double> oldVector = List.of();
        final List<Double> newVector = List.of();

        assertTrue(new PageRankCounter().isAcceptableEps(oldVector, newVector, epsilon));
    }

    @Test
    void testCountPageRankOneInEdgePerVertex() {
        final Graph graph = new Graph(Map.of(0, List.of(2), 1, List.of(0), 2, List.of(1)),
                Map.of(0, 1, 1, 1, 2, 1));

        final List<Double> initPageRanks = List.of(1d/3d, 1d/3d, 1d/3d);
        final double epsilon = 0.0001;
        final int vertexesAmount = 3;
        final List<Double> expectedPageRanks = List.of(0.3333d, 0.3333d, 0.3333d);

        final List<Double> actualPageRanks = new PageRankCounter()
                .countPageRank(graph, initPageRanks, epsilon, DAMPING_FACTOR, vertexesAmount);

        assertArrayEquals(expectedPageRanks.toArray(), actualPageRanks.stream()
                .mapToDouble(e -> round(e * ANSWER_SCALE) / ANSWER_SCALE)
                .boxed()
                .toArray()
        );
    }

    @Test
    void testCountPageRankParallelOneInEdgePerVertex() {
        final Graph graph = new Graph(Map.of(0, List.of(2), 1, List.of(0), 2, List.of(1)),
                Map.of(0, 1, 1, 1, 2, 1));

        final List<Double> initPageRanks = List.of(1d/3d, 1d/3d, 1d/3d);
        final double epsilon = 0.0001;
        final int vertexesAmount = 3;
        final List<Double> expectedPageRanks = List.of(0.3333d, 0.3333d, 0.3333d);

        final List<Double> actualPageRanks = new ParallelPageRankCounter()
                .countPageRank(graph, initPageRanks, epsilon, DAMPING_FACTOR, vertexesAmount);

        assertArrayEquals(expectedPageRanks.toArray(), actualPageRanks.stream()
                .mapToDouble(e -> round(e * ANSWER_SCALE) / ANSWER_SCALE)
                .boxed()
                .toArray()
        );
    }

    @Test
    void testCountPageRankOneVertexHasTwoOutEdges() {
        final Graph graph = new Graph(Map.of(0, List.of(2), 1, List.of(0, 2), 2, List.of(1)),
                Map.of(0, 1, 1, 1, 2, 2));

        final List<Double> initPageRanks = List.of(1d/3d, 1d/3d, 1d/3d);
        final double epsilon = 0.0001;
        final int vertexesAmount = 3;
        final List<Double> expectedPageRanks = List.of(0.2314d, 0.3933d, 0.3753d);

        final List<Double> actualPageRanks = new PageRankCounter()
                .countPageRank(graph, initPageRanks, epsilon, DAMPING_FACTOR, vertexesAmount);

        assertArrayEquals(expectedPageRanks.toArray(), actualPageRanks.stream()
                .mapToDouble(e -> round(e * ANSWER_SCALE) / ANSWER_SCALE)
                .boxed()
                .toArray()
        );
    }

    @Test
    void testCountPageRankParallelOneVertexHasTwoOutEdges() {
        final Graph graph = new Graph(Map.of(0, List.of(2), 1, List.of(0, 2), 2, List.of(1)),
                Map.of(0, 1, 1, 1, 2, 2));

        final List<Double> initPageRanks = List.of(1d/3d, 1d/3d, 1d/3d);
        final double epsilon = 0.0001;
        final int vertexesAmount = 3;
        final List<Double> expectedPageRanks = List.of(0.2314d, 0.3933d, 0.3753d);

        final List<Double> actualPageRanks = new ParallelPageRankCounter()
                .countPageRank(graph, initPageRanks, epsilon, DAMPING_FACTOR, vertexesAmount);

        assertArrayEquals(expectedPageRanks.toArray(), actualPageRanks.stream()
                .mapToDouble(e -> round(e * ANSWER_SCALE) / ANSWER_SCALE)
                .boxed()
                .toArray()
        );
    }

    @Test
    void testCountPageRankAlmostFullGraphTwoVertexesAreNotConnected() {
        final Graph graph = new Graph(Map.of(0, List.of(1), 1, List.of(0,2), 2, List.of(1)),
                                        Map.of(0, 1, 1, 2, 2, 1));


        final List<Double> initPageRanks = List.of(1d/3d, 1d/3d, 1d/3d);
        final double epsilon = 0.0001;
        final int vertexesAmount = 3;
        final List<Double> expectedPageRanks = List.of(0.2647d, 0.4706d, 0.2647d);

        final List<Double> actualPageRanks = new PageRankCounter()
                .countPageRank(graph, initPageRanks, epsilon, DAMPING_FACTOR, vertexesAmount);

        assertArrayEquals(expectedPageRanks.toArray(), actualPageRanks.stream()
                .mapToDouble(e -> round(e * ANSWER_SCALE) / ANSWER_SCALE)
                .boxed()
                .toArray()
        );
    }

    @Test
    void testCountPageRankParallelAlmostFullGraphTwoVertexesAreNotConnected() {
        final Graph graph = new Graph(Map.of(0, List.of(1), 1, List.of(0,2), 2, List.of(1)),
                Map.of(0, 1, 1, 2, 2, 1));


        final List<Double> initPageRanks = List.of(1d/3d, 1d/3d, 1d/3d);
        final double epsilon = 0.0001;
        final int vertexesAmount = 3;
        final List<Double> expectedPageRanks = List.of(0.2647d, 0.4706d, 0.2647d);

        final List<Double> actualPageRanks = new ParallelPageRankCounter()
                .countPageRank(graph, initPageRanks, epsilon, DAMPING_FACTOR, vertexesAmount);

        assertArrayEquals(expectedPageRanks.toArray(), actualPageRanks.stream()
                .mapToDouble(e -> round(e * ANSWER_SCALE) / ANSWER_SCALE)
                .boxed()
                .toArray()
        );
    }
}