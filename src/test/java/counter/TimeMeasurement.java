package counter;

import dataset.DatasetPreProcessor;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TimeMeasurement {
    private static final double EPSILON = 0.000001;
    private static final float DAMPING_FACTOR = 0.85f;
    private static final int TESTS_AMOUNT = 100;

    public static void main(String[] args) throws IOException {
        countTime(new PageRankCounter(), "SERIAL_");
        countTime(new ParallelPageRankCounter(), "PARALLEL_");
    }

    public static void countTime(PageRankCounter counter, String filePrefix) throws IOException {
        for (DatasetPreProcessor.DatasetName datasetName : DatasetPreProcessor.DatasetName.values()) {
            System.out.println(datasetName + " started:");
            Graph graph = null;
            int graphSize = 0;
            switch (datasetName) {
                case STANFORD:
                    graph = DatasetPreProcessor.getStanfordData();
                    graphSize = DatasetPreProcessor.DatasetSize.STANFORD.getSize();
                    break;
                case NOTERDAME:
                    graph = DatasetPreProcessor.getNoterDameData();
                    graphSize = DatasetPreProcessor.DatasetSize.NOTERDAME.getSize();
                    break;
                case GOOGLE:
                    graph = DatasetPreProcessor.getGoogleData();
                    graphSize = DatasetPreProcessor.DatasetSize.GOOGLE.getSize();
                    break;
                case BERKLY:
                    graph = DatasetPreProcessor.getBerklyData();
                    graphSize = DatasetPreProcessor.DatasetSize.BERKLY.getSize();
                    break;
            }

            final List<Double> initPR = new ArrayList<>(Collections.nCopies(graphSize, 1d/graphSize));

            try(FileWriter writer = new FileWriter(filePrefix + datasetName.name())) {
                for (int i = 0; i < TESTS_AMOUNT; i++) {
                    System.out.println(filePrefix + datasetName + ": " + (double)i/(double)TESTS_AMOUNT*100 + "%");
                    long start = System.currentTimeMillis();
                    counter.countPageRank(graph, initPR, EPSILON, DAMPING_FACTOR, graphSize);
                    long finish = System.currentTimeMillis();

                    long workTime = finish - start;
                    writer.write(Long.toString(workTime));
                    writer.append('\n');
                }
            }
        }
    }
}
