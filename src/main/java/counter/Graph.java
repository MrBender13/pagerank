package counter;

import java.util.List;
import java.util.Map;

public class Graph {
    private final Map<Integer, List<Integer>> incomingEdges;
    private final Map<Integer, Integer> outgoingEdgesAmount;
    
    public Graph(Map<Integer,List<Integer>> incomingEdges, Map<Integer, Integer> outgoingEdgesAmount) {
        this.incomingEdges = incomingEdges;
        this.outgoingEdgesAmount = outgoingEdgesAmount;
    }
    
    List<Integer> getIncomingEdges(int nodeIndex) {
        return incomingEdges.getOrDefault(nodeIndex, List.of());
    }

    int getOutgoingEdgesAmount(int nodeIndex) {
        return outgoingEdgesAmount.getOrDefault(nodeIndex, 0);
    }
}
