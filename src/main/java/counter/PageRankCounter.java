package counter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Math.abs;

//todo rewrite javadoc
public class PageRankCounter {

    /**
     * <p>This is a pure recursive method, which counts PageRank values for given web graph.</p>
     * @param graph given web graph in view of immutable map.
     *              Where key is node index and value is list of incoming edges.
     * @param oldPageRanks unmodifiable list of old PageRank values.
     *                    Those values should be initial or passed from previous method call.
     * @param eps given Epsilon value. It is used to stop algorithm when it gets close enough to real PageRank values.
     * @param damping value to avoid problem of "dangling nodes", usually it is 0,85.
     * @param N amount of nodes in graph.
     * @return unmodifiable list of PageRank values for a given graph.
     * @see <a href=https://www.ams.org/publicoutreach/feature-column/fcarc-pagerank/a> for detailed algo description
     */
    public List<Double> countPageRank(final Graph graph, final List<Double> oldPageRanks,
                                                final double eps, final float damping, final int N) {
        final List<Double> newPageRanks = IntStream.range(0, N)
                .sequential()
                .mapToObj(graph::getIncomingEdges)
                .mapToDouble(incomingEdges -> countSimpleRank(graph, incomingEdges, oldPageRanks))
                .map(simpleRank -> addDampingFactor(simpleRank, damping, N))
                .boxed()
                .collect(Collectors.toUnmodifiableList());

        return isAcceptableEps(oldPageRanks, newPageRanks, eps) ? newPageRanks
                : countPageRank(graph, newPageRanks, eps, damping, N);
    }

    /**
     * <p>This is a pure method, which counts simple rank value for given vertex of graph. Simple means that rank
     * is calculated without damping factor.</p>
     * @param incomingIds unmodifiable list of edges to given vertex.
     * @param prevPageRanks unmodifiable list of current PageRank values of graph's vertexes.
     * @return double value of simple rank of given vertex.
     */
     double countSimpleRank(final Graph graph, final List<Integer> incomingIds, final List<Double> prevPageRanks) {
        return incomingIds.stream().sequential()
                .mapToDouble(id -> prevPageRanks.get(id) / graph.getOutgoingEdgesAmount(id))
                .sum();
    }

    /**
     * <p>This is a pure method for adding damping factor to PageRank formula.</p>
     * @param simpleRank value of simple rank of the given vertex.
     * @param dampingFactor value of damping factor coefficient.
     * @param nodesAmount amount of nodes in web graph.
     * @return double value of PageRank of given vertex.
     */
    double addDampingFactor(final double simpleRank, final float dampingFactor, final int nodesAmount) {
        return (1d - dampingFactor) / nodesAmount + dampingFactor * simpleRank;
    }

    /**
     * <p>This is a pure method, which checks if two lists with PageRank values differ no more than given Epsilon</p>
     * <p>This method should be called after every new iteration of counting PageRank values.</p>
     * @param oldPageRanks unmodifiable list with old PageRank values.
     * @param newPageRanks unmodifiable list with new PageRank values.
     * @param eps Epsilon on which two lists with PageRank values should differ.
     * @return true if difference between two lists is less than Epsilon, else - false.
     */
     boolean isAcceptableEps(final List<Double> oldPageRanks,
                                           final List<Double> newPageRanks, final double eps) {
         return IntStream.range(0, oldPageRanks.size()).sequential()
                 .allMatch(i-> abs(oldPageRanks.get(i)-newPageRanks.get(i))< eps);
    }
}