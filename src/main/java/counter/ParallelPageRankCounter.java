package counter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ParallelPageRankCounter extends PageRankCounter{

    @Override
    public List<Double> countPageRank(Graph graph, List<Double> oldPageRanks, double eps, float damping, int N) {
        final List<Double> newPageRanks = IntStream.range(0, N)
                .parallel()
                .mapToObj(graph::getIncomingEdges)
                .mapToDouble(incomingEdges -> countSimpleRank(graph, incomingEdges, oldPageRanks))
                .map(simpleRank -> addDampingFactor(simpleRank, damping, N))
                .boxed()
                .collect(Collectors.toUnmodifiableList());

        return isAcceptableEps(oldPageRanks, newPageRanks, eps) ? newPageRanks
                : countPageRank(graph, newPageRanks, eps, damping, N);
    }
}
