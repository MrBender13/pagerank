package dataset;

import counter.Graph;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class DatasetPreProcessor {
    private static final String RESOURCES_PATH = "/home/mrbender/IdeaProjects/pagerank/src/main/resources/";

    public static void main(String[] args) throws IOException {
        System.out.println(Files.lines(Paths.get("SERIAL_STANFORD"))
                .mapToInt(Integer::valueOf)
                .average().getAsDouble());
    }

    private static Map<Integer, List<Integer>> readIncomingEdges(String path, int maxIndex) throws IOException {
        return Files.lines(Paths.get(path), StandardCharsets.UTF_8)
                .skip(4)
                .collect(groupingBy(e -> Integer.valueOf(e.split("	")[1]),
                        mapping(e -> Integer.valueOf(e.split("	")[0]), toUnmodifiableList())));
    }

    private static Map<Integer, Integer> readOutEdgesAmount(String path, int maxIndex) throws IOException {
        Map<Integer, List<Integer>> outEdges = Files.lines(Paths.get(path), StandardCharsets.UTF_8)
                .skip(4)
                .collect(groupingBy(e -> Integer.valueOf(e.split("	")[0]),
                        mapping(e -> Integer.valueOf(e.split("	")[1]), toUnmodifiableList())));
        return outEdges.entrySet().stream()
                .collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, e -> e.getValue().size()));
    }

    public static Graph getBerklyData() throws IOException {
        return new Graph(readIncomingEdges(RESOURCES_PATH + DatasetName.BERKLY.name, DatasetSize.BERKLY.size),
                readOutEdgesAmount(RESOURCES_PATH + DatasetName.BERKLY.name, DatasetSize.BERKLY.size));
    }

    public static Graph getGoogleData() throws IOException {
        return new Graph(readIncomingEdges(RESOURCES_PATH + DatasetName.GOOGLE.name, DatasetSize.GOOGLE.size),
                readOutEdgesAmount(RESOURCES_PATH + DatasetName.GOOGLE.name, DatasetSize.GOOGLE.size));
    }

    public static Graph getNoterDameData() throws IOException {
        return new Graph(readIncomingEdges(RESOURCES_PATH + DatasetName.NOTERDAME.name, DatasetSize.NOTERDAME.size),
                readOutEdgesAmount(RESOURCES_PATH + DatasetName.NOTERDAME.name, DatasetSize.NOTERDAME.size));
    }

    public static Graph getStanfordData() throws IOException {
        return new Graph(readIncomingEdges(RESOURCES_PATH + DatasetName.STANFORD.name, DatasetSize.STANFORD.size),
                readOutEdgesAmount(RESOURCES_PATH + DatasetName.STANFORD.name, DatasetSize.STANFORD.size));
    }

    public enum DatasetName {
        STANFORD("web-Stanford.txt"),
        BERKLY("web-BerkStan.txt"),
        GOOGLE("web-Google.txt"),
        NOTERDAME("web-NotreDame.txt");

        private String name;

        DatasetName(String name) {
            this.name = name;
        }
    }

    public enum DatasetSize {
        STANFORD(281_904),
        BERKLY(685_231),
        GOOGLE(916_428),
        NOTERDAME(325_730);

        public int getSize() {
            return size;
        }

        private int size;

        DatasetSize(int size) {
            this.size = size;
        }
    }
}
